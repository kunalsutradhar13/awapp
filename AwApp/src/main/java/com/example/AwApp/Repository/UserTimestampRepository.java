package com.example.AwApp.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.AwApp.model.UserTimestamp;

public interface UserTimestampRepository extends JpaRepository<UserTimestamp, String> {

	@Query(value = "SELECT t FROM UserTimestamp t WHERE t.username = ?1 ")
	List<UserTimestamp> findUserTimeStampByName(String name);

	@Query(value = "SELECT t FROM UserTimestamp t order by t.createdAt desc ")
	public List<UserTimestamp> findAllByOrderByDateAsc(Pageable pageable);

}
