package com.example.AwApp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.AwApp.model.ActionCount;
import com.example.AwApp.model.GameScore;
import com.example.AwApp.model.PlayerGameActionScore;

@Repository
public interface PlayerGameActionScoreRepository extends JpaRepository<PlayerGameActionScore, String> {

	@Query(value = "SELECT COUNT(DISTINCT t.action) FROM PlayerGameActionScore t WHERE t.playername=?1 and t.game=?2")
	int findActionCountForPlayerAndGame(String playername, String game);

	@Query(value = "SELECT COUNT(t.score) FROM PlayerGameActionScore t WHERE t.playername!= ?1 and t.game=?2")
	int findGameCountForEveryPlayerAndGame(String playername, String game);

	@Query(value = "SELECT DISTINCT t.playername FROM PlayerGameActionScore t WHERE t.game=?1")
	List<String> findUniquePlayersForGame(String game);

	@Query(value = "SELECT COUNT(t.score) FROM PlayerGameActionScore t WHERE t.playername= ?1 and t.game=?2")
	int findGameCountForGivenPlayerAndGame(String playername, String game);

	@Query(value = "SELECT new com.example.AwApp.model.ActionCount(t.action, COUNT(*)) FROM PlayerGameActionScore t WHERE t.playername = ?1 and t.game=?2 GROUP BY t.action")
	List<ActionCount> findActionCountForPlayer(String playername, String game);

}
