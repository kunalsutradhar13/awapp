package com.example.AwApp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.AwApp.model.UserActionLog;

@Repository
public interface UserActionLogRepository extends JpaRepository<UserActionLog, String> {

	@Query(value = "SELECT t FROM UserActionLog t WHERE t.username = ?1 ")
	List<UserActionLog> findUserActionLogByName(String name);

	@Query(value = "SELECT count(t.id) FROM UserActionLog t WHERE t.action = ?1 and t.username=?2")
	int findActionCountForUSername(String action, String name);

	@Query(value = "SELECT count(t.id) FROM UserActionLog t WHERE t.action = ?1")
	int findActionCount(String action);
	
	@Query(value = "SELECT count(t.id) FROM UserActionLog t")
	int findActionCountWithNoName();

}
