package com.example.AwApp.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.AwApp.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

	@Query(value = "SELECT t FROM User t WHERE t.username = ?1 ")
	User findUserByName(String name);
}
