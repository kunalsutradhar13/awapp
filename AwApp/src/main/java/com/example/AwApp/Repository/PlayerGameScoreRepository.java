package com.example.AwApp.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.AwApp.model.*;
import java.util.*;

@Repository
public interface PlayerGameScoreRepository extends JpaRepository<PlayerGameScore, String> {

	@Query(value = "SELECT COUNT(t.playername) FROM PlayerGameScore t WHERE t.playername = ?1 and t.game=?2")
	int findGameCountForPlayerAndGame(String playername, String game);

	@Query(value = "SELECT COUNT(*) FROM PlayerGameScore t WHERE t.playername != ?1 and t.game=?2")
	int findGameCountForEveryPlayerAndGame(String playername, String game);

	@Query(value = "SELECT distinct t.playername FROM PlayerGameScore t WHERE t.playername NOT IN ?1")
	List<String> findUniquePlayers(List<String> names);

	@Query(value = "SELECT distinct t.game FROM PlayerGameScore t WHERE t.playername=?1")
	List<String> findUniqueGamesPlayedByPlayer(String name);

	@Query(value = "SELECT new com.example.AwApp.model.GameScore(t.game, sum(t.score)) FROM PlayerGameScore t WHERE t.playername = ?1 GROUP BY t.game")
	List<GameScore> findGameScoresForPlayers(String playername);
	
	@Query(value = "SELECT new com.example.AwApp.model.CircleGameScore(t.game, sum(t.score)) FROM PlayerGameScore t WHERE t.playername = ?1 GROUP BY t.game")
	List<CircleGameScore> findCircleGameScoresForPlayers(String playername);
	
	@Query(value = "SELECT distinct(t.playername) FROM PlayerGameScore t")
	List<String> getAllPlayers();

}
