package com.example.AwApp.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.AwApp.Repository.PlayerGameActionScoreRepository;
import com.example.AwApp.Repository.PlayerGameScoreRepository;
import com.example.AwApp.Repository.UserActionLogRepository;
import com.example.AwApp.Repository.UserRepository;
import com.example.AwApp.Repository.UserTimestampRepository;
import com.example.AwApp.Utils.JaccardSimilarity;
import com.example.AwApp.model.ActionCount;
import com.example.AwApp.model.CircleGameScore;
import com.example.AwApp.model.FlareWrapper;
import com.example.AwApp.model.GameForCircle;
import com.example.AwApp.model.GameScore;
import com.example.AwApp.model.PlayerGameActionScore;
import com.example.AwApp.model.PlayerGameScore;
import com.example.AwApp.model.PlayerWithGame;
import com.example.AwApp.model.User;
import com.example.AwApp.model.UserActionLog;
import com.example.AwApp.model.UserTimestamp;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MainController {

	@Autowired
	UserRepository noteRepository;

	@Autowired
	UserTimestampRepository utRepository;

	@Autowired
	UserActionLogRepository uaRepository;

	@Autowired
	PlayerGameActionScoreRepository pgasRepository;

	@Autowired
	PlayerGameScoreRepository pgsRepository;

	@RequestMapping("/hello")
	String crunchifyURL() {
		return "Hello Friends! ";
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@Validated
	@ResponseBody
	public ResponseEntity<String> Check(@RequestBody Map<String, String> payload, HttpServletRequest request)
			throws Exception {

		try {
			Map<String, String[]> map = request.getParameterMap();
			// Reading the Map
			// Works for GET && POST Method
			boolean correct_username = false;
			boolean correct_password = false;
			for (String paramName : payload.keySet()) {
				if (paramName.equalsIgnoreCase("username")) {
					User t = noteRepository.findUserByName(payload.get(paramName));
					if (t != null) {
						correct_username = true;
						if (payload.keySet().contains("password")) {
							if (t.getPassword().equalsIgnoreCase(payload.get("password"))) {
								correct_password = true;
								Calendar cal = Calendar.getInstance();
								String dateInString = new java.text.SimpleDateFormat("EEEE, dd/MM/yyyy/hh:mm:ss")
										.format(cal.getTime());
								SimpleDateFormat formatter = new SimpleDateFormat("EEEE, dd/MM/yyyy/hh:mm:ss");
								Date dt = formatter.parse(dateInString);
								UserTimestamp ut = new UserTimestamp(new String(payload.get(paramName)), dt);
								utRepository.save(ut);
							} else {
								return new ResponseEntity<>("Incorrect password", HttpStatus.BAD_REQUEST);
							}
						} else {
							return new ResponseEntity<>("Incorrect password", HttpStatus.BAD_REQUEST);
						}
					} else {
						return new ResponseEntity<>("Incorrect username", HttpStatus.BAD_REQUEST);
					}
				}
			}
			if (correct_username && correct_password) {
				return new ResponseEntity<>("good username and password", HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		}

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/usersCreate", method = RequestMethod.POST)
	@Validated
	@ResponseBody
	public ResponseEntity<String> create(@RequestBody Map<String, String> payload, HttpServletRequest request)
			throws Exception {

		try {
			Map<String, String[]> map = request.getParameterMap();
			// Reading the Map
			// Works for GET && POST Method
			Date dt = new Date();
			String username = new String();
			String password = new String();
			String date = new String();
			for (String paramName : payload.keySet()) {
				if (paramName.equalsIgnoreCase("username")) {
					username = new String(payload.get(paramName));
				}
				if (paramName.equalsIgnoreCase("password")) {
					password = new String(payload.get(paramName));
				}
				if (paramName.equalsIgnoreCase("timeStamp")) {
					date = new String(payload.get(paramName));
					Calendar cal = Calendar.getInstance();
					String dateInString = new java.text.SimpleDateFormat("EEEE, dd/MM/yyyy/hh:mm:ss")
							.format(cal.getTime());
					SimpleDateFormat formatter = new SimpleDateFormat("EEEE, dd/MM/yyyy/hh:mm:ss");
					dt = formatter.parse(dateInString);
				}

			}
			if (username != null && password != null && date != null) {
				User note = new User(username, password, dt);
				noteRepository.save(note);
			}
			return new ResponseEntity<>("created", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("Already exists", HttpStatus.BAD_REQUEST);

		}

	}

	@CrossOrigin
	@RequestMapping(value = "/users", method = RequestMethod.POST)
	@Validated
	@ResponseBody
	public ResponseEntity<String> SaveEvent(@RequestBody Map<String, String> payload, HttpServletRequest request)
			throws Exception {

		try {
			boolean allSaved = false;
			for (String paramName : payload.keySet()) {
				if (paramName.equalsIgnoreCase("username")) {
					String st = new String(payload.get(paramName));
					if (st != null && !st.isEmpty()) {
						User t = noteRepository.findUserByName(payload.get(paramName));
						if (t != null) {
							Calendar cal = Calendar.getInstance();
							String dateInString = new java.text.SimpleDateFormat("EEEE, dd/MM/yyyy/hh:mm:ss")
									.format(cal.getTime());
							SimpleDateFormat formatter = new SimpleDateFormat("EEEE, dd/MM/yyyy/hh:mm:ss");
							Date dt = formatter.parse(dateInString);
							if (payload.keySet().contains("perfAction")) {
								if (payload.get("perfAction") != null && !payload.get("perfAction").isEmpty()) {
									UserActionLog ut = new UserActionLog(new String(payload.get(paramName)),
											new String(payload.get("perfAction")), dt);
									uaRepository.save(ut);
									allSaved = true;
								} else {
									return new ResponseEntity<>("Incorrect Username", HttpStatus.BAD_REQUEST);
								}
							} else {
								return new ResponseEntity<>("Incorrect Username", HttpStatus.BAD_REQUEST);
							}
						} else {
							return new ResponseEntity<>("Incorrect Username", HttpStatus.BAD_REQUEST);
						}
					}
				}
			}
			if (allSaved) {
				return new ResponseEntity<>("actual", HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		}
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/getRecentLogin", method = RequestMethod.GET)
	public ResponseEntity<String> getRecentLogin() {
		try {
			List<UserTimestamp> utList = utRepository.findAllByOrderByDateAsc(new PageRequest(0, 1));
			UserTimestamp ut = utList.get(0);
			if (ut == null) {
				return new ResponseEntity<>("", HttpStatus.OK);
			} else {
				return new ResponseEntity<>(ut.getUsername(), HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>("", HttpStatus.OK);
		}

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/getActionCountForRecentLogin", method = RequestMethod.GET)
	public ResponseEntity<String> getActionCountForRecentLogin() {
		try {
			List<UserTimestamp> utList = utRepository.findAllByOrderByDateAsc(new PageRequest(0, 1));
			UserTimestamp ut = utList.get(0);
			Map<String, String> payload = new HashMap<>();
			payload.put("Post Solutions",
					String.valueOf(uaRepository.findActionCountForUSername("answer_posted", ut.getUsername())));
			payload.put("Post Questions",
					String.valueOf(uaRepository.findActionCountForUSername("question_posted", ut.getUsername())));
			payload.put("Upvote Answer",
					String.valueOf(uaRepository.findActionCountForUSername("vote_up", ut.getUsername())));
			payload.put("Downvote Answer",
					String.valueOf(uaRepository.findActionCountForUSername("vote_down", ut.getUsername())));
			payload.put("Star a Question",
					String.valueOf(uaRepository.findActionCountForUSername("star_up_and_down", ut.getUsername())));

			String json = new ObjectMapper().writeValueAsString(payload);
			System.out.println(json);

			if (json == null) {
				return new ResponseEntity<>(json, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(json, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>("", HttpStatus.OK);
		}

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/getAccountLoginHistory", method = RequestMethod.GET)
	public ResponseEntity<String> getAccountLoginHistory() {
		try {
			List<UserTimestamp> utList = utRepository.findAllByOrderByDateAsc(new PageRequest(0, 1));
			UserTimestamp ut = utList.get(0);

			List<UserTimestamp> utListT = utRepository.findUserTimeStampByName(ut.getUsername());
			StringBuilder st = new StringBuilder();
			for (UserTimestamp uts : utListT) {
				DateFormat dateFormat = new SimpleDateFormat("EEEE, dd/MM/yyyy/hh:mm:ss");

				String strDate = dateFormat.format(uts.getCreatedAt());
				st.append(strDate);
				st.append("_");
			}

			if (st.toString() != null) {
				return new ResponseEntity<>(st.toString(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("none", HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>("none", HttpStatus.OK);
		}

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/getActionCount", method = RequestMethod.GET)
	public ResponseEntity<String> getActionCount() {
		try {
			Map<String, String> payload = new HashMap<>();
			payload.put("Post Solutions", String.valueOf(uaRepository.findActionCount("answer_posted")));
			payload.put("Post Questions", String.valueOf(uaRepository.findActionCount("question_posted")));
			payload.put("Upvote Answer", String.valueOf(uaRepository.findActionCount("vote_up")));
			payload.put("Downvote Answer", String.valueOf(uaRepository.findActionCount("vote_down")));
			payload.put("Star a Question", String.valueOf(uaRepository.findActionCount("star_up_and_down")));

			String json = new ObjectMapper().writeValueAsString(payload);
			System.out.println(json);

			if (json == null) {
				return new ResponseEntity<>(json, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(json, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>("", HttpStatus.OK);
		}

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/isAnyActionRecorded", method = RequestMethod.GET)
	public ResponseEntity<String> isAnyActionRecorded() {
		try {
			int count = uaRepository.findActionCountWithNoName();
			if (count == 0) {
				return new ResponseEntity<>("none", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("exist", HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>("none", HttpStatus.OK);
		}

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/getGameScoreForUsers", method = RequestMethod.GET)
	public List<PlayerWithGame> getGameScoreForUsers() {
		List<PlayerWithGame> result = new ArrayList<>();

		System.out.println("hello ");

		try {

			List<String> playerNames = pgsRepository.getAllPlayers();

			System.out.println("hello " + playerNames);

			for (String pName : playerNames) {
				PlayerWithGame pg = new PlayerWithGame();

				long score = 0L;

				pg.setPlayerName(pName);

				List<GameScore> gameScoreList = pgsRepository.findGameScoresForPlayers(pName);

				for (GameScore game : gameScoreList) {

					List<ActionCount> actionCountList = pgasRepository.findActionCountForPlayer(pName,
							game.getGameName());

					System.out.println("action count list" + actionCountList);
					game.setGameActions(actionCountList);

					score += game.getGameScore();
				}

				pg.setGameScores(gameScoreList);

				pg.setTotalScore(score);

				result.add(pg);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;

	}	
	
	
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/getCircleData", method = RequestMethod.GET)
	public FlareWrapper getCircleData() {
		
		List<GameForCircle> result = new ArrayList<>();
		
		System.out.println("hello " );

		try {
			
			
			List<String> playerNames = pgsRepository.getAllPlayers();
			
			System.out.println("hello " + playerNames);

			
			for(String pName : playerNames) {
				GameForCircle pg = new GameForCircle();
				
				
				pg.setName(pName);
				
				List<CircleGameScore> gameScoreList = pgsRepository.findCircleGameScoresForPlayers(pName);
				
				
//				for(CircleGameScore game : gameScoreList) {
//					
//					
//					game.setGameActions(actionCountList);
//
//					score += game.getGameScore();
//				}
//				
				pg.setChildren(gameScoreList);
				
				
				result.add(pg);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new FlareWrapper("flare", result);

	}
	
	

	@CrossOrigin
	@RequestMapping(value = "/savePlayerGameScore", method = RequestMethod.POST)
	@Validated
	@ResponseBody
	public ResponseEntity<String> savePlayerGameScore(@RequestBody Map<String, String> payload,
			HttpServletRequest request) throws Exception {

		try {
			boolean allSaved = false;
			for (String paramName : payload.keySet()) {
				if (paramName.equalsIgnoreCase("playername")) {
					String st = new String(payload.get(paramName));
					if (st != null && !st.isEmpty()) {
						if (payload.keySet().contains("game") && payload.get("game") != null
								&& !payload.get("game").isEmpty()) {
							if (payload.keySet().contains("action") && payload.get("action") != null
									&& !payload.get("action").isEmpty()) {
								if (payload.keySet().contains("score") && payload.get("score") != null
										&& !payload.get("score").isEmpty()) {
									PlayerGameActionScore pgas = new PlayerGameActionScore(st, payload.get("game"),
											payload.get("action"), Integer.parseInt(payload.get("score")));
									pgasRepository.save(pgas);

									if (payload.get("game").equalsIgnoreCase("game1")
											|| payload.get("game").equalsIgnoreCase("game2")) {
										if (pgsRepository.findGameCountForPlayerAndGame(st, payload.get("game")) == 0
												&& pgasRepository.findActionCountForPlayerAndGame(st,
														payload.get("game")) == 2) {
											PlayerGameScore pgs = new PlayerGameScore(st, payload.get("game"),
													Integer.parseInt(payload.get("score")));
											pgsRepository.save(pgs);
										}
									} else if (payload.get("game").equalsIgnoreCase("game3")) {
										if (pgsRepository.findGameCountForPlayerAndGame(st, payload.get("game")) == 0
												&& pgasRepository.findActionCountForPlayerAndGame(st,
														payload.get("game")) == 4) {
											PlayerGameScore pgs = new PlayerGameScore(st, payload.get("game"),
													Integer.parseInt(payload.get("score")));
											pgsRepository.save(pgs);
										}
									}

									allSaved = true;
								} else {
									return new ResponseEntity<>("Incorrect Score", HttpStatus.BAD_REQUEST);
								}
							} else {
								return new ResponseEntity<>("Incorrect action", HttpStatus.BAD_REQUEST);
							}
						} else {
							return new ResponseEntity<>("Incorrect game", HttpStatus.BAD_REQUEST);
						}
					}
				}
			}
			if (allSaved) {
				return new ResponseEntity<>("actual", HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/getSuggestedPlayers", method = RequestMethod.GET)
	public ResponseEntity<String> getSuggestedPlayers(
			@RequestParam(value = "playername", required = false) String playername) {
		try {

			JaccardSimilarity.mainItems = new ArrayList<int[]>();
			JaccardSimilarity.sortedTransactions = new ArrayList<int[]>();

			List<String> inputNames = new ArrayList<String>();
			inputNames.add(playername);
			List<String> outputNames = pgsRepository.findUniquePlayers(inputNames);

			Map<Integer, List<String>> gameCountNames = new HashMap<Integer, List<String>>();
			for (String name : outputNames) {
				List<String> gamesPlayed = pgsRepository.findUniqueGamesPlayedByPlayer(name);
				int gameCount = 0;
				for (String game : gamesPlayed) {
					if (game.equalsIgnoreCase("game1")) {
						gameCount += 1;
					} else if (game.equalsIgnoreCase("game2")) {
						gameCount += 3;
					} else if (game.equalsIgnoreCase("game3")) {
						gameCount += 5;
					}
				}
				JaccardSimilarity.mainItems.add(new int[] { gameCount, gamesPlayed.size() });
				List<String> mapNames = gameCountNames.getOrDefault(gameCount, new ArrayList<String>());
				mapNames.add(name);
				gameCountNames.put(gameCount, mapNames);
			}

			List<String> gamesPlayed = pgsRepository.findUniqueGamesPlayedByPlayer(playername);
			int gameCount = 0;
			for (String game : gamesPlayed) {
				if (game.equalsIgnoreCase("game1")) {
					gameCount += 1;
				} else if (game.equalsIgnoreCase("game2")) {
					gameCount += 3;
				} else if (game.equalsIgnoreCase("game3")) {
					gameCount += 5;
				}
			}

			JaccardSimilarity.generatePairs(new int[] { gameCount, gamesPlayed.size() });

			int playerKind = JaccardSimilarity.sortedTransactions.get(1)[0];

			Map<String, String> payload = new HashMap<>();

			for (int i = 0; i < Math.min(gameCountNames.get(playerKind).size(), 4); i++) {
				payload.put(String.valueOf(i), String.valueOf(gameCountNames.get(playerKind).get(i)));
			}

			String json = new ObjectMapper().writeValueAsString(payload);
			System.out.println(json);

			JaccardSimilarity.mainItems = null;
			JaccardSimilarity.sortedTransactions = null;
			if (json == null) {
				return new ResponseEntity<>(json, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(json, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>("", HttpStatus.OK);
		}

	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/getSuggestedGames", method = RequestMethod.GET)
	public ResponseEntity<String> getSuggestedGames(
			@RequestParam(value = "playername", required = false) String playername) {

		try {
			Map<String, String> payload = new HashMap<>();
			int k = 0;
			String[] games = new String[] { "game1", "game2", "game3" };
			for (int i = 0; i < games.length; i++) {
				List<String> names = pgasRepository.findUniquePlayersForGame(games[i]);
				int[] gameScoresForPlayers = new int[names.size()];
				int j = 0;
				for (String name : names) {
					int totalActionCount = pgasRepository.findGameCountForGivenPlayerAndGame(name, games[i]);
					gameScoresForPlayers[j++] = totalActionCount;
				}
				Arrays.sort(gameScoresForPlayers);
				double median;
				if (gameScoresForPlayers.length % 2 == 0)
					median = ((double) gameScoresForPlayers[gameScoresForPlayers.length / 2]
							+ (double) gameScoresForPlayers[gameScoresForPlayers.length / 2 - 1]) / 2;
				else
					median = (double) gameScoresForPlayers[gameScoresForPlayers.length / 2];

				int playerCountForGame = pgasRepository.findGameCountForGivenPlayerAndGame(playername, games[i]);
				if (median >= playerCountForGame) {
					payload.put(String.valueOf(k), games[i]);
					k++;
				}

			}

			String json = new ObjectMapper().writeValueAsString(payload);
			System.out.println(json);

			if (json == null) {
				return new ResponseEntity<>(json, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(json, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>("", HttpStatus.OK);
		}
	}
}
