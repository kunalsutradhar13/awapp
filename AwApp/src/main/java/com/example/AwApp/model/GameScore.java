package com.example.AwApp.model;

import java.util.List;

public class GameScore {
	
	public GameScore(String gameName, Long gameScore) {
		super();
		GameName = gameName;
		GameScore = gameScore;
	}
	String GameName;
	Long GameScore;
	List<ActionCount> gameActions;
	
	public String getGameName() {
		return GameName;
	}
	public void setGameName(String gameName) {
		GameName = gameName;
	}
	public Long getGameScore() {
		return GameScore;
	}
	public void setGameScore(Long gameScore) {
		GameScore = gameScore;
	}
	public List<ActionCount> getGameActions() {
		return gameActions;
	}
	public void setGameActions(List<ActionCount> gameActions) {
		this.gameActions = gameActions;
	}

	

}
