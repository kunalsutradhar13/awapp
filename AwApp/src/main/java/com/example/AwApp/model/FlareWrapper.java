package com.example.AwApp.model;

import java.util.List;

public class FlareWrapper {
	
	public FlareWrapper() {
		
	}
	
	public FlareWrapper(String name, List<GameForCircle> children) {
		super();
		this.name = name;
		this.children = children;
	}
	String name;
	List<GameForCircle> children;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<GameForCircle> getChildren() {
		return children;
	}
	public void setChildren(List<GameForCircle> children) {
		this.children = children;
	}

}
