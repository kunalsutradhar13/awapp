package com.example.AwApp.model;

public class CircleGameScore {
	public CircleGameScore(String name, Long size) {
		super();
		this.name = name;
		this.size = size;
	}
	String name;
	Long size;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
}
