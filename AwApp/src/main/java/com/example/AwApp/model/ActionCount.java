package com.example.AwApp.model;

public class ActionCount {
	public ActionCount(String actionName, Long actionCount) {
		super();
		this.actionName = actionName;
		this.actionCount = actionCount;
	}
	String actionName;
	Long actionCount;
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public Long getActionCount() {
		return actionCount;
	}
	public void setActionCount(Long actionCount) {
		this.actionCount = actionCount;
	}

}
