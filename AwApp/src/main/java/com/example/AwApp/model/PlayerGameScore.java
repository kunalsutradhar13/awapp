package com.example.AwApp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.example.AwApp.Utils.IdGenerator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "PlayerGameScore")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class PlayerGameScore implements Serializable {

	public PlayerGameScore() {

	}

	public PlayerGameScore(String playername, String game, Integer score) {
		this.id = IdGenerator.generateUniqueId();
		this.playername = playername;
		this.game = game;
		this.score = Long.valueOf(score.intValue());
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "playername")
	@NotBlank
	private String playername;

	@Column(name = "game")
	@NotBlank
	private String game;

	@Column(name = "score")
	@NotNull
	private Long score;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlayername() {
		return playername;
	}

	public void setPlayername(String playername) {
		this.playername = playername;
	}

	public String getGame() {
		return game;
	}

	public void setGame(String game) {
		this.game = game;
	}

	public Long getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = Long.valueOf(score.intValue());
	}

}
