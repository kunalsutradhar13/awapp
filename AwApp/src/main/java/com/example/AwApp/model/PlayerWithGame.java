package com.example.AwApp.model;

import java.util.*;

public class PlayerWithGame {
	
	String playerName;
	
	List<GameScore> gameScores;

	
	long totalScore;

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public List<GameScore> getGameScores() {
		return gameScores;
	}

	public void setGameScores(List<GameScore> gameScores) {
		this.gameScores = gameScores;
	}

	public long getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(long totalScore) {
		this.totalScore = totalScore;
	}
	

}
