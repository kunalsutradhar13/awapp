package com.example.AwApp.model;

import java.util.List;

public class GameForCircle {
	
	public GameForCircle() {

	}
	public GameForCircle(String name, List<CircleGameScore> children) {
		super();
		this.name = name;
		this.children = children;
	}
	String name;
	List<CircleGameScore> children;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<CircleGameScore> getChildren() {
		return children;
	}
	public void setChildren(List<CircleGameScore> children) {
		this.children = children;
	}
}
