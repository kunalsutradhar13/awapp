package com.example.AwApp.Utils;

import java.util.*;

public class JaccardSimilarity {

	 static public List<int[]> mainItems ;
	 static public ArrayList<int[]> sortedTransactions;

//	static {
//		mainItems = new ArrayList<int[]>();
//
//		mainItems.add(new int[] { 1, 1 });
//		mainItems.add(new int[] { 3, 1 });
//		mainItems.add(new int[] { 5, 1 });
//		mainItems.add(new int[] { 4, 2 });
//		mainItems.add(new int[] { 6, 2 });
//		mainItems.add(new int[] { 8, 2 });
//		mainItems.add(new int[] { 9, 3 });
//
//	}

	static private double jaccardSimilarity(int[] a, int[] b) {

		Set<Integer> s1 = new HashSet<Integer>();
		for (int i = 0; i < a.length; i++) {
			s1.add(a[i]);
		}
		Set<Integer> s2 = new HashSet<Integer>();
		for (int i = 0; i < b.length; i++) {
			s2.add(b[i]);
		}

		final int sa = s1.size();
		final int sb = s2.size();
		s1.retainAll(s2);
		final int intersection = s1.size();
		return 1d / (sa + sb - intersection) * intersection;
	}
//
//	public static void main(String args[]) {
//		
//		int[] input = new int[] {5,2};
//		generatePairs(input);
//		for (int i=0;i<sortedTransactions.size();i++) {
//			System.out.println(sortedTransactions.get(i).length);
//			System.out.println(sortedTransactions.get(i)[0]);
//			System.out.println(sortedTransactions.get(i)[1]);
//		}
//		
//	}

	static private boolean isAllEqual(List<Double> a) {

		for (int i = 1; i < a.size(); i++) {
			if (a.get(0) != a.get(i)) {
				return false;
			}
		}

		return true;
	}

	static public void generatePairs(int[] input) {

		// for (int i = 0; i < mainItems.size() - 1; i++) {

		// if (!sortedTransactions.contains(mainItems.get(i))) {

		if (!sortedTransactions.contains(input)) {

			List<Double> myd = new ArrayList<Double>();
			List<int[]> mys = new ArrayList<int[]>();

			for (int j = 0; j < mainItems.size(); j++) {

				if (!sortedTransactions.contains(mainItems.get(j))) {

					myd.add(jaccardSimilarity(input, mainItems.get(j)));
					mys.add(mainItems.get(j));
				}
			}

			if (isAllEqual(myd) == false) {

				sortedTransactions.add(input);
				sortedTransactions.add(mys.get(maxValue(myd)));
			}
		}
		// }
	}

	static private int maxValue(List<Double> d) {

		double max = d.get(0);
		int f = 0;

		for (int i = 1; i < d.size(); i++) {

			if (d.get(i) > max) {

				max = d.get(i);
				f = i;
			}
		}
		return f;
	}
}