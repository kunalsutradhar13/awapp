package com.example.AwApp.Utils;

import java.util.UUID;

public class IdGenerator {

	public static long generateUniqueId() {
		UUID idOne = UUID.randomUUID();
		String str = "" + idOne;
		int uid = str.hashCode();
		String filterStr = "" + uid;
		str = filterStr.replaceAll("-", "");
		return Long.parseLong(str);
	}

	// XXX: replace with java.util.UUID

}