-- MySQL dump 10.13  Distrib 8.0.12, for macos10.13 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dummy_user`
--

DROP TABLE IF EXISTS `dummy_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dummy_user` (
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dummy_user`
--

LOCK TABLES `dummy_user` WRITE;
/*!40000 ALTER TABLE `dummy_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `dummy_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notes`
--

LOCK TABLES `notes` WRITE;
/*!40000 ALTER TABLE `notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `username` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `id` bigint(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('aaa','2018-09-20 13:20:35',871446769,'123'),('bbb','2018-09-20 13:46:09',30500657,'123'),('ccc','2018-09-20 13:48:42',74617036,'123');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_action_log`
--

DROP TABLE IF EXISTS `user_action_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_action_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=381 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_action_log`
--

LOCK TABLES `user_action_log` WRITE;
/*!40000 ALTER TABLE `user_action_log` DISABLE KEYS */;
INSERT INTO `user_action_log` VALUES (1,'normal_click','2018-09-20 13:43:36','aaa'),(2,'normal_click','2018-09-20 13:43:36','aaa'),(3,'normal_click','2018-09-20 13:43:47','aaa'),(4,'normal_click','2018-09-20 13:43:47','aaa'),(5,'normal_click','2018-09-20 13:43:48','aaa'),(6,'normal_click','2018-09-20 13:43:53','aaa'),(7,'answer_posted','2018-09-20 13:44:47','aaa'),(8,'normal_click','2018-09-20 13:44:47','aaa'),(9,'normal_click','2018-09-20 13:44:48','aaa'),(10,'normal_click','2018-09-20 13:44:48','aaa'),(11,'normal_click','2018-09-20 13:44:48','aaa'),(12,'answer_posted','2018-09-20 13:44:48','aaa'),(13,'normal_click','2018-09-20 13:44:48','aaa'),(14,'answer_posted','2018-09-20 13:44:49','aaa'),(15,'normal_click','2018-09-20 13:44:49','aaa'),(16,'answer_posted','2018-09-20 13:44:49','aaa'),(17,'normal_click','2018-09-20 13:44:49','aaa'),(18,'answer_posted','2018-09-20 13:44:49','aaa'),(19,'normal_click','2018-09-20 13:44:49','aaa'),(20,'answer_posted','2018-09-20 13:44:49','aaa'),(21,'normal_click','2018-09-20 13:44:49','aaa'),(22,'normal_click','2018-09-20 13:44:49','aaa'),(23,'answer_posted','2018-09-20 13:44:49','aaa'),(24,'answer_posted','2018-09-20 13:44:50','aaa'),(25,'normal_click','2018-09-20 13:44:50','aaa'),(26,'normal_click','2018-09-20 13:44:50','aaa'),(27,'answer_posted','2018-09-20 13:44:50','aaa'),(28,'answer_posted','2018-09-20 13:44:50','aaa'),(29,'normal_click','2018-09-20 13:44:50','aaa'),(30,'answer_posted','2018-09-20 13:44:50','aaa'),(31,'normal_click','2018-09-20 13:44:50','aaa'),(32,'answer_posted','2018-09-20 13:44:51','aaa'),(33,'normal_click','2018-09-20 13:44:51','aaa'),(34,'answer_posted','2018-09-20 13:44:51','aaa'),(35,'normal_click','2018-09-20 13:44:51','aaa'),(36,'answer_posted','2018-09-20 13:44:51','aaa'),(37,'normal_click','2018-09-20 13:44:51','aaa'),(38,'normal_click','2018-09-20 13:44:51','aaa'),(39,'answer_posted','2018-09-20 13:44:51','aaa'),(40,'answer_posted','2018-09-20 13:44:51','aaa'),(41,'normal_click','2018-09-20 13:44:51','aaa'),(42,'answer_posted','2018-09-20 13:44:51','aaa'),(43,'normal_click','2018-09-20 13:44:51','aaa'),(44,'answer_posted','2018-09-20 13:44:52','aaa'),(45,'normal_click','2018-09-20 13:44:52','aaa'),(46,'normal_click','2018-09-20 13:45:05','aaa'),(47,'normal_click','2018-09-20 13:45:06','aaa'),(48,'question_posted','2018-09-20 13:45:07','aaa'),(49,'question_posted','2018-09-20 13:45:09','aaa'),(50,'question_posted','2018-09-20 13:45:09','aaa'),(51,'question_posted','2018-09-20 13:45:09','aaa'),(52,'question_posted','2018-09-20 13:45:09','aaa'),(53,'question_posted','2018-09-20 13:45:09','aaa'),(54,'question_posted','2018-09-20 13:45:09','aaa'),(55,'question_posted','2018-09-20 13:45:10','aaa'),(56,'question_posted','2018-09-20 13:45:10','aaa'),(57,'question_posted','2018-09-20 13:45:10','aaa'),(58,'question_posted','2018-09-20 13:45:10','aaa'),(59,'question_posted','2018-09-20 13:45:10','aaa'),(60,'question_posted','2018-09-20 13:45:10','aaa'),(61,'question_posted','2018-09-20 13:45:11','aaa'),(62,'question_posted','2018-09-20 13:45:11','aaa'),(63,'question_posted','2018-09-20 13:45:11','aaa'),(64,'question_posted','2018-09-20 13:45:11','aaa'),(65,'question_posted','2018-09-20 13:45:11','aaa'),(66,'vote_up','2018-09-20 13:45:28','aaa'),(67,'normal_click','2018-09-20 13:45:28','aaa'),(68,'normal_click','2018-09-20 13:45:29','aaa'),(69,'vote_up','2018-09-20 13:45:29','aaa'),(70,'vote_up','2018-09-20 13:45:29','aaa'),(71,'normal_click','2018-09-20 13:45:29','aaa'),(72,'vote_up','2018-09-20 13:45:29','aaa'),(73,'normal_click','2018-09-20 13:45:29','aaa'),(74,'vote_up','2018-09-20 13:45:29','aaa'),(75,'normal_click','2018-09-20 13:45:29','aaa'),(76,'vote_up','2018-09-20 13:45:29','aaa'),(77,'normal_click','2018-09-20 13:45:29','aaa'),(78,'vote_up','2018-09-20 13:45:29','aaa'),(79,'normal_click','2018-09-20 13:45:29','aaa'),(80,'vote_up','2018-09-20 13:45:30','aaa'),(81,'normal_click','2018-09-20 13:45:30','aaa'),(82,'vote_up','2018-09-20 13:45:30','aaa'),(83,'normal_click','2018-09-20 13:45:30','aaa'),(84,'vote_up','2018-09-20 13:45:30','aaa'),(85,'normal_click','2018-09-20 13:45:30','aaa'),(86,'vote_up','2018-09-20 13:45:30','aaa'),(87,'normal_click','2018-09-20 13:45:30','aaa'),(88,'vote_down','2018-09-20 13:45:31','aaa'),(89,'normal_click','2018-09-20 13:45:31','aaa'),(90,'vote_down','2018-09-20 13:45:31','aaa'),(91,'normal_click','2018-09-20 13:45:31','aaa'),(92,'vote_down','2018-09-20 13:45:31','aaa'),(93,'normal_click','2018-09-20 13:45:31','aaa'),(94,'vote_down','2018-09-20 13:45:32','aaa'),(95,'normal_click','2018-09-20 13:45:32','aaa'),(96,'vote_down','2018-09-20 13:45:32','aaa'),(97,'normal_click','2018-09-20 13:45:32','aaa'),(98,'vote_down','2018-09-20 13:45:32','aaa'),(99,'normal_click','2018-09-20 13:45:32','aaa'),(100,'vote_down','2018-09-20 13:45:32','aaa'),(101,'normal_click','2018-09-20 13:45:32','aaa'),(102,'normal_click','2018-09-20 13:45:32','aaa'),(103,'vote_down','2018-09-20 13:45:32','aaa'),(104,'vote_down','2018-09-20 13:45:32','aaa'),(105,'normal_click','2018-09-20 13:45:32','aaa'),(106,'star_up_and_down','2018-09-20 13:45:33','aaa'),(107,'normal_click','2018-09-20 13:45:33','aaa'),(108,'star_up_and_down','2018-09-20 13:45:33','aaa'),(109,'normal_click','2018-09-20 13:45:33','aaa'),(110,'star_up_and_down','2018-09-20 13:45:33','aaa'),(111,'normal_click','2018-09-20 13:45:33','aaa'),(112,'star_up_and_down','2018-09-20 13:45:33','aaa'),(113,'normal_click','2018-09-20 13:45:33','aaa'),(114,'star_up_and_down','2018-09-20 13:45:34','aaa'),(115,'normal_click','2018-09-20 13:45:34','aaa'),(116,'star_up_and_down','2018-09-20 13:45:34','aaa'),(117,'normal_click','2018-09-20 13:45:34','aaa'),(118,'star_up_and_down','2018-09-20 13:45:34','aaa'),(119,'normal_click','2018-09-20 13:45:34','aaa'),(120,'star_up_and_down','2018-09-20 13:45:34','aaa'),(121,'normal_click','2018-09-20 13:45:34','aaa'),(122,'star_up_and_down','2018-09-20 13:45:34','aaa'),(123,'normal_click','2018-09-20 13:45:34','aaa'),(124,'star_up_and_down','2018-09-20 13:45:34','aaa'),(125,'normal_click','2018-09-20 13:45:34','aaa'),(126,'star_up_and_down','2018-09-20 13:45:35','aaa'),(127,'normal_click','2018-09-20 13:45:35','aaa'),(128,'star_up_and_down','2018-09-20 13:45:35','aaa'),(129,'normal_click','2018-09-20 13:45:35','aaa'),(130,'star_up_and_down','2018-09-20 13:45:35','aaa'),(131,'normal_click','2018-09-20 13:45:35','aaa'),(132,'star_up_and_down','2018-09-20 13:45:35','aaa'),(133,'normal_click','2018-09-20 13:45:35','aaa'),(134,'star_up_and_down','2018-09-20 13:45:35','aaa'),(135,'normal_click','2018-09-20 13:45:35','aaa'),(136,'vote_down','2018-09-20 13:45:36','aaa'),(137,'normal_click','2018-09-20 13:45:36','aaa'),(138,'vote_down','2018-09-20 13:45:36','aaa'),(139,'normal_click','2018-09-20 13:45:36','aaa'),(140,'vote_down','2018-09-20 13:45:37','aaa'),(141,'normal_click','2018-09-20 13:45:37','aaa'),(142,'vote_down','2018-09-20 13:45:37','aaa'),(143,'normal_click','2018-09-20 13:45:37','aaa'),(144,'vote_down','2018-09-20 13:45:37','aaa'),(145,'normal_click','2018-09-20 13:45:37','aaa'),(146,'vote_down','2018-09-20 13:45:37','aaa'),(147,'normal_click','2018-09-20 13:45:37','aaa'),(148,'vote_up','2018-09-20 13:45:38','aaa'),(149,'normal_click','2018-09-20 13:45:38','aaa'),(150,'vote_up','2018-09-20 13:45:38','aaa'),(151,'normal_click','2018-09-20 13:45:38','aaa'),(152,'vote_up','2018-09-20 13:45:38','aaa'),(153,'normal_click','2018-09-20 13:45:38','aaa'),(154,'vote_up','2018-09-20 13:45:38','aaa'),(155,'normal_click','2018-09-20 13:45:38','aaa'),(156,'vote_up','2018-09-20 13:45:38','aaa'),(157,'normal_click','2018-09-20 13:45:38','aaa'),(158,'vote_up','2018-09-20 13:47:21','aaa'),(159,'normal_click','2018-09-20 13:47:21','aaa'),(160,'vote_up','2018-09-20 13:47:21','aaa'),(161,'normal_click','2018-09-20 13:47:21','aaa'),(162,'vote_up','2018-09-20 13:47:21','bbb'),(163,'normal_click','2018-09-20 13:47:21','bbb'),(164,'vote_up','2018-09-20 13:47:21','bbb'),(165,'normal_click','2018-09-20 13:47:21','bbb'),(166,'vote_up','2018-09-20 13:47:21','bbb'),(167,'normal_click','2018-09-20 13:47:21','bbb'),(168,'vote_up','2018-09-20 13:47:22','bbb'),(169,'normal_click','2018-09-20 13:47:22','bbb'),(170,'vote_up','2018-09-20 13:47:22','bbb'),(171,'normal_click','2018-09-20 13:47:22','bbb'),(172,'vote_up','2018-09-20 13:47:22','bbb'),(173,'normal_click','2018-09-20 13:47:22','bbb'),(174,'vote_up','2018-09-20 13:47:22','bbb'),(175,'normal_click','2018-09-20 13:47:22','bbb'),(176,'vote_up','2018-09-20 13:47:22','bbb'),(177,'normal_click','2018-09-20 13:47:22','bbb'),(178,'vote_down','2018-09-20 13:47:23','bbb'),(179,'normal_click','2018-09-20 13:47:23','bbb'),(180,'vote_down','2018-09-20 13:47:23','bbb'),(181,'normal_click','2018-09-20 13:47:23','bbb'),(182,'vote_down','2018-09-20 13:47:23','bbb'),(183,'normal_click','2018-09-20 13:47:23','bbb'),(184,'vote_down','2018-09-20 13:47:23','bbb'),(185,'normal_click','2018-09-20 13:47:23','bbb'),(186,'vote_down','2018-09-20 13:47:24','bbb'),(187,'normal_click','2018-09-20 13:47:24','bbb'),(188,'vote_down','2018-09-20 13:47:24','bbb'),(189,'normal_click','2018-09-20 13:47:24','bbb'),(190,'vote_down','2018-09-20 13:47:24','bbb'),(191,'normal_click','2018-09-20 13:47:24','bbb'),(192,'vote_down','2018-09-20 13:47:24','bbb'),(193,'normal_click','2018-09-20 13:47:24','bbb'),(194,'star_up_and_down','2018-09-20 13:47:25','bbb'),(195,'normal_click','2018-09-20 13:47:25','bbb'),(196,'star_up_and_down','2018-09-20 13:47:26','bbb'),(197,'normal_click','2018-09-20 13:47:26','bbb'),(198,'star_up_and_down','2018-09-20 13:47:26','bbb'),(199,'normal_click','2018-09-20 13:47:26','bbb'),(200,'star_up_and_down','2018-09-20 13:47:26','bbb'),(201,'normal_click','2018-09-20 13:47:26','bbb'),(202,'star_up_and_down','2018-09-20 13:47:26','bbb'),(203,'normal_click','2018-09-20 13:47:26','bbb'),(204,'star_up_and_down','2018-09-20 13:47:26','bbb'),(205,'normal_click','2018-09-20 13:47:26','bbb'),(206,'star_up_and_down','2018-09-20 13:47:27','bbb'),(207,'normal_click','2018-09-20 13:47:27','bbb'),(208,'star_up_and_down','2018-09-20 13:47:27','bbb'),(209,'normal_click','2018-09-20 13:47:27','bbb'),(210,'vote_up','2018-09-20 13:47:27','bbb'),(211,'normal_click','2018-09-20 13:47:27','bbb'),(212,'vote_up','2018-09-20 13:47:28','bbb'),(213,'normal_click','2018-09-20 13:47:28','bbb'),(214,'vote_up','2018-09-20 13:47:28','bbb'),(215,'normal_click','2018-09-20 13:47:28','bbb'),(216,'vote_up','2018-09-20 13:47:28','bbb'),(217,'normal_click','2018-09-20 13:47:28','bbb'),(218,'vote_up','2018-09-20 13:47:28','bbb'),(219,'normal_click','2018-09-20 13:47:28','bbb'),(220,'vote_down','2018-09-20 13:47:29','bbb'),(221,'normal_click','2018-09-20 13:47:29','bbb'),(222,'vote_down','2018-09-20 13:47:29','bbb'),(223,'normal_click','2018-09-20 13:47:29','bbb'),(224,'vote_down','2018-09-20 13:47:29','bbb'),(225,'normal_click','2018-09-20 13:47:29','bbb'),(226,'vote_down','2018-09-20 13:47:29','bbb'),(227,'normal_click','2018-09-20 13:47:29','bbb'),(228,'vote_down','2018-09-20 13:47:29','bbb'),(229,'normal_click','2018-09-20 13:47:29','bbb'),(230,'star_up_and_down','2018-09-20 13:47:31','bbb'),(231,'normal_click','2018-09-20 13:47:31','bbb'),(232,'star_up_and_down','2018-09-20 13:47:31','bbb'),(233,'normal_click','2018-09-20 13:47:31','bbb'),(234,'star_up_and_down','2018-09-20 13:47:31','bbb'),(235,'normal_click','2018-09-20 13:47:31','bbb'),(236,'normal_click','2018-09-20 13:47:31','bbb'),(237,'star_up_and_down','2018-09-20 13:47:31','bbb'),(238,'star_up_and_down','2018-09-20 13:47:31','bbb'),(239,'normal_click','2018-09-20 13:47:31','bbb'),(240,'normal_click','2018-09-20 13:49:00','bbb'),(241,'answer_posted','2018-09-20 13:49:04','ccc'),(242,'normal_click','2018-09-20 13:49:04','ccc'),(243,'normal_click','2018-09-20 13:49:04','ccc'),(244,'answer_posted','2018-09-20 13:49:04','ccc'),(245,'normal_click','2018-09-20 13:49:04','ccc'),(246,'answer_posted','2018-09-20 13:49:04','ccc'),(247,'answer_posted','2018-09-20 13:49:04','ccc'),(248,'normal_click','2018-09-20 13:49:04','ccc'),(249,'answer_posted','2018-09-20 13:49:05','ccc'),(250,'normal_click','2018-09-20 13:49:05','ccc'),(251,'answer_posted','2018-09-20 13:49:05','ccc'),(252,'normal_click','2018-09-20 13:49:05','ccc'),(253,'answer_posted','2018-09-20 13:49:05','ccc'),(254,'normal_click','2018-09-20 13:49:05','ccc'),(255,'answer_posted','2018-09-20 13:49:05','ccc'),(256,'normal_click','2018-09-20 13:49:05','ccc'),(257,'normal_click','2018-09-20 13:49:05','ccc'),(258,'answer_posted','2018-09-20 13:49:05','ccc'),(259,'answer_posted','2018-09-20 13:49:06','ccc'),(260,'normal_click','2018-09-20 13:49:06','ccc'),(261,'normal_click','2018-09-20 13:49:06','ccc'),(262,'answer_posted','2018-09-20 13:49:06','ccc'),(263,'answer_posted','2018-09-20 13:49:06','ccc'),(264,'normal_click','2018-09-20 13:49:06','ccc'),(265,'answer_posted','2018-09-20 13:49:06','ccc'),(266,'normal_click','2018-09-20 13:49:06','ccc'),(267,'answer_posted','2018-09-20 13:49:06','ccc'),(268,'normal_click','2018-09-20 13:49:06','ccc'),(269,'answer_posted','2018-09-20 13:49:06','ccc'),(270,'normal_click','2018-09-20 13:49:06','ccc'),(271,'answer_posted','2018-09-20 13:49:07','ccc'),(272,'normal_click','2018-09-20 13:49:07','ccc'),(273,'answer_posted','2018-09-20 13:49:07','ccc'),(274,'normal_click','2018-09-20 13:49:07','ccc'),(275,'answer_posted','2018-09-20 13:49:07','ccc'),(276,'normal_click','2018-09-20 13:49:07','ccc'),(277,'answer_posted','2018-09-20 13:49:07','ccc'),(278,'normal_click','2018-09-20 13:49:07','ccc'),(279,'answer_posted','2018-09-20 13:49:07','ccc'),(280,'normal_click','2018-09-20 13:49:07','ccc'),(281,'answer_posted','2018-09-20 13:49:07','ccc'),(282,'normal_click','2018-09-20 13:49:07','ccc'),(283,'answer_posted','2018-09-20 13:49:08','ccc'),(284,'normal_click','2018-09-20 13:49:08','ccc'),(285,'answer_posted','2018-09-20 13:49:08','ccc'),(286,'normal_click','2018-09-20 13:49:08','ccc'),(287,'answer_posted','2018-09-20 13:49:08','ccc'),(288,'normal_click','2018-09-20 13:49:08','ccc'),(289,'answer_posted','2018-09-20 13:49:08','ccc'),(290,'normal_click','2018-09-20 13:49:08','ccc'),(291,'normal_click','2018-09-20 13:49:10','ccc'),(292,'normal_click','2018-09-20 13:49:15','ccc'),(293,'normal_click','2018-09-20 13:49:16','ccc'),(294,'question_posted','2018-09-20 13:49:18','ccc'),(295,'question_posted','2018-09-20 13:49:19','ccc'),(296,'question_posted','2018-09-20 13:49:19','ccc'),(297,'question_posted','2018-09-20 13:49:19','ccc'),(298,'question_posted','2018-09-20 13:49:20','ccc'),(299,'question_posted','2018-09-20 13:49:20','ccc'),(300,'question_posted','2018-09-20 13:49:20','ccc'),(301,'question_posted','2018-09-20 13:49:20','ccc'),(302,'question_posted','2018-09-20 13:49:21','ccc'),(303,'question_posted','2018-09-20 13:49:21','ccc'),(304,'question_posted','2018-09-20 13:49:21','ccc'),(305,'question_posted','2018-09-20 13:49:21','ccc'),(306,'question_posted','2018-09-20 13:49:21','ccc'),(307,'question_posted','2018-09-20 13:49:21','ccc'),(308,'question_posted','2018-09-20 13:49:22','ccc'),(309,'question_posted','2018-09-20 13:49:22','ccc'),(310,'vote_down','2018-09-20 13:49:30','ccc'),(311,'vote_down','2018-09-20 13:49:30','ccc'),(312,'normal_click','2018-09-20 13:49:30','ccc'),(313,'vote_down','2018-09-20 13:49:30','ccc'),(314,'normal_click','2018-09-20 13:49:30','ccc'),(315,'vote_down','2018-09-20 13:49:31','ccc'),(316,'normal_click','2018-09-20 13:49:31','ccc'),(317,'vote_down','2018-09-20 13:49:31','ccc'),(318,'normal_click','2018-09-20 13:49:31','ccc'),(319,'vote_down','2018-09-20 13:49:31','ccc'),(320,'normal_click','2018-09-20 13:49:31','ccc'),(321,'vote_down','2018-09-20 13:49:31','ccc'),(322,'normal_click','2018-09-20 13:49:31','ccc'),(323,'star_up_and_down','2018-09-20 13:49:32','ccc'),(324,'normal_click','2018-09-20 13:49:32','ccc'),(325,'star_up_and_down','2018-09-20 13:49:32','ccc'),(326,'normal_click','2018-09-20 13:49:32','ccc'),(327,'star_up_and_down','2018-09-20 13:49:32','ccc'),(328,'normal_click','2018-09-20 13:49:32','ccc'),(329,'vote_down','2018-09-20 13:49:32','ccc'),(330,'normal_click','2018-09-20 13:49:32','ccc'),(331,'vote_down','2018-09-20 13:49:33','ccc'),(332,'normal_click','2018-09-20 13:49:33','ccc'),(333,'vote_down','2018-09-20 13:49:33','ccc'),(334,'normal_click','2018-09-20 13:49:33','ccc'),(335,'vote_down','2018-09-20 13:49:33','ccc'),(336,'normal_click','2018-09-20 13:49:33','ccc'),(337,'vote_down','2018-09-20 13:49:33','ccc'),(338,'normal_click','2018-09-20 13:49:33','ccc'),(339,'vote_down','2018-09-20 13:49:33','ccc'),(340,'normal_click','2018-09-20 13:49:33','ccc'),(341,'vote_down','2018-09-20 13:49:34','ccc'),(342,'normal_click','2018-09-20 13:49:34','ccc'),(343,'vote_down','2018-09-20 13:49:34','ccc'),(344,'normal_click','2018-09-20 13:49:34','ccc'),(345,'vote_down','2018-09-20 13:49:34','ccc'),(346,'normal_click','2018-09-20 13:49:34','ccc'),(347,'vote_down','2018-09-20 13:49:34','ccc'),(348,'normal_click','2018-09-20 13:49:34','ccc'),(349,'star_up_and_down','2018-09-20 13:49:35','ccc'),(350,'normal_click','2018-09-20 13:49:35','ccc'),(351,'star_up_and_down','2018-09-20 13:49:35','ccc'),(352,'normal_click','2018-09-20 13:49:35','ccc'),(353,'star_up_and_down','2018-09-20 13:49:35','ccc'),(354,'normal_click','2018-09-20 13:49:35','ccc'),(355,'star_up_and_down','2018-09-20 13:49:35','ccc'),(356,'normal_click','2018-09-20 13:49:35','ccc'),(357,'star_up_and_down','2018-09-20 13:49:35','ccc'),(358,'normal_click','2018-09-20 13:49:35','ccc'),(359,'vote_up','2018-09-20 13:49:36','ccc'),(360,'normal_click','2018-09-20 13:49:36','ccc'),(361,'vote_up','2018-09-20 13:49:36','ccc'),(362,'normal_click','2018-09-20 13:49:36','ccc'),(363,'vote_up','2018-09-20 13:49:37','ccc'),(364,'normal_click','2018-09-20 13:49:37','ccc'),(365,'vote_up','2018-09-20 13:49:37','ccc'),(366,'normal_click','2018-09-20 13:49:37','ccc'),(367,'vote_up','2018-09-20 13:49:37','ccc'),(368,'normal_click','2018-09-20 13:49:37','ccc'),(369,'vote_up','2018-09-20 13:49:37','ccc'),(370,'normal_click','2018-09-20 13:49:37','ccc'),(371,'vote_up','2018-09-20 13:49:37','ccc'),(372,'normal_click','2018-09-20 13:49:37','ccc'),(373,'vote_up','2018-09-20 13:49:37','ccc'),(374,'normal_click','2018-09-20 13:49:37','ccc'),(375,'vote_up','2018-09-20 13:49:38','ccc'),(376,'normal_click','2018-09-20 13:49:38','ccc'),(377,'normal_click','2018-09-20 13:49:38','ccc'),(378,'vote_up','2018-09-20 13:49:38','ccc'),(379,'vote_up','2018-09-20 13:49:38','ccc'),(380,'normal_click','2018-09-20 13:49:38','ccc');
/*!40000 ALTER TABLE `user_action_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_timestamp`
--

DROP TABLE IF EXISTS `user_timestamp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_timestamp` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_timestamp`
--

LOCK TABLES `user_timestamp` WRITE;
/*!40000 ALTER TABLE `user_timestamp` DISABLE KEYS */;
INSERT INTO `user_timestamp` VALUES (1,'2018-09-20 13:20:44','aaa'),(2,'2018-09-20 13:44:39','aaa'),(3,'2018-09-20 13:45:53','aaa'),(4,'2018-09-20 13:47:15','bbb'),(5,'2018-09-20 13:48:15','aaa'),(6,'2018-09-20 13:48:24','bbb'),(7,'2018-09-20 13:48:51','ccc'),(8,'2018-09-20 13:50:25','ccc'),(9,'2018-09-20 13:50:36','bbb'),(10,'2018-09-20 13:50:58','aaa');
/*!40000 ALTER TABLE `user_timestamp` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-20 14:03:09
