function validate(){
	var username = document.getElementById("username").value;
	var password = document.getElementById("password").value;
		var currentdate = new Date();
		var datetime = currentdate.getDate() + "/"
		+ (currentdate.getMonth()+1)  + "/" 
		+ currentdate.getFullYear() + " @ "  
		+ currentdate.getHours() + ":"  
		+ currentdate.getMinutes() + ":" 
		+ currentdate.getSeconds();


		var url  = "https://localhost:8443/api/login";
		var data = {};
		data.username = username;
		data.password  = password;
		data.timeStamp = datetime;
		var json = JSON.stringify(data);
		console.log(json)


		var http = new XMLHttpRequest();
		http.open('POST', url, true);

		//Send the proper header information along with the request
		http.setRequestHeader('Content-type','application/json; charset=utf-8');

		http.onreadystatechange = function() {//Call a function when the state changes.
		    if(http.status == 200) {
		    		 console.log("Login Successful " + username)
		    		 localStorage.setItem("storageName",username);
		    		 location.href = "https://localhost:8443/success.html";
		         document.getElementById("loginspan").innerHTML="login  successful";
		    }else{
		    	 document.getElementById("loginspan").innerHTML="login unsuccessful, username or password invalid.";
		    	 localStorage.setItem("storageName","");
		    }
		}
		http.send(json);


		// var xhr = new XMLHttpRequest();
		// xhr.open("POST", url, true);
		// xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
		// xhr.onload = function () {
		// var users = JSON.parse(xhr.responseText);
		// if (xhr.readyState == 4 && xhr.status == "201") {
		// 	console.table(users);
		// } else {
		// 	console.error(users);
		// }
		// }
		// xhr.send(json);
	// window.location = "success.html"; // Redirecting to other page.
}


