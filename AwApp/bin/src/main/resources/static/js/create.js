function create(){
	var username = document.getElementById("username1").value;
	var password1 = document.getElementById("password1").value;
	var password2 = document.getElementById("password2").value;
	if ( password1 == password2){
		var currentdate = new Date();
		var datetime = currentdate.getDate() + "/"
		+ (currentdate.getMonth()+1)  + "/" 
		+ currentdate.getFullYear() + " @ "  
		+ currentdate.getHours() + ":"  
		+ currentdate.getMinutes() + ":" 
		+ currentdate.getSeconds();

		console.log("creation " + username+password1+ datetime)
		var url  = "http://localhost:8080/api/usersCreate";
		var data = {};
		data.username = username;
		data.password  = password1;
		data.timeStamp = datetime;
		var json = JSON.stringify(data);
		console.log(json)


		var http = new XMLHttpRequest();
		http.open('POST', url, true);

		//Send the proper header information along with the request
		http.setRequestHeader('Content-type','application/json; charset=utf-8');

		http.onreadystatechange = function() {//Call a function when the state changes.
		    if(http.readyState == 4 && http.status == 200) {
		    	document.getElementById("myspan").innerHTML="Account creation successful, please go to <a href='index.html'>login</a> page";
		    }else{
		    	document.getElementById("myspan").innerHTML="Account creation unsuccessful, try different username";
		    }
		}
		http.send(json);


		// var xhr = new XMLHttpRequest();
		// xhr.open("POST", url, true);
		// xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
		// xhr.onload = function () {
		// var users = JSON.parse(xhr.responseText);
		// if (xhr.readyState == 4 && xhr.status == "201") {
		// 	console.table(users);
		// } else {
		// 	console.error(users);
		// }
		// }
		// xhr.send(json);
	// window.location = "success.html"; // Redirecting to other page.
	}else{
		document.getElementById("myspan").innerHTML="passwords different, try again";
	}
}
